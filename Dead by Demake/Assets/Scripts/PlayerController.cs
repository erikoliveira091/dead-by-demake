using UnityEngine;
using UnityEngine.InputSystem;

public enum PlayerState
{
    Idle,
    Walking,
    Decode,
    Captured
};

public class PlayerController : MonoBehaviour
{
    Vector3 mousePosition;
    public SimpleAIMovement killer;

    //Configurações do Player
    BoxCollider2D bc;
    Rigidbody2D rb;
    public PlayerState currState;

    [SerializeField]
    private float maxSpeed = 15, acceleration = 50, deacceleration = 100;
    [SerializeField]
    private float currentSpeed = 0;

    private Vector2 oldMovementInput;
    private Vector2 movementInput { get; set; }

    [SerializeField] InputActionReference movement;

    [SerializeField] SpriteRenderer sprite;
    [SerializeField] Animator anim;

    public bool _facingRight = true;
    public int _facingDirection = 1;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();
        currState = PlayerState.Idle;
    }

    private void Update()
    {
        switch (currState)
        {
            case (PlayerState.Idle):
                acceleration = 50;
                anim.SetBool("isWalking", false);
                anim.SetBool("isRepairing", false);
                break;
            case (PlayerState.Walking):
                anim.SetBool("isWalking", true);
                anim.SetBool("isRepairing", false);
                break;
            
            case (PlayerState.Decode):
                acceleration = 1;
                anim.SetBool("isWalking", false);
                anim.SetBool("isRepairing", true);
                break;

            case (PlayerState.Captured):
                this.transform.position = (killer.transform.position + new Vector3(-0.1f,-0.1f,0));
                this.sprite.transform.rotation = Quaternion.Euler(0, 0, 90.0f);
                this.bc.enabled = false;
                anim.SetBool("isWalking", false);
                anim.SetBool("isRepairing", true);
                break;
        }

        movementInput = movement.action.ReadValue<Vector2>();

        
        if (killer.isPlayerGetted)
        {
            currState = PlayerState.Captured;
        } 
        else if (rb.velocity.x == 0 && rb.velocity.y == 0)
        {
            currState = PlayerState.Idle;
        }
        else
        {
            currState = PlayerState.Walking;
        }
        
        

        CheckDirection();
    }

    private void FixedUpdate()
    {

        if (movementInput.magnitude > 0 && currentSpeed >= 0)
        {
            oldMovementInput = movementInput.normalized;
            currentSpeed += acceleration * maxSpeed * Time.deltaTime;
        }
        else
        {
            currentSpeed -= deacceleration * maxSpeed * Time.deltaTime;

        }
        currentSpeed = Mathf.Clamp(currentSpeed, 0, maxSpeed);
        rb.velocity = oldMovementInput * currentSpeed;
    }

    private void CheckDirection()
    {
        if (movementInput.x > 0 && !_facingRight)
        {
            Flip();
        }
        else if (movementInput.x < 0 && _facingRight)
        {
            Flip();
        }
    }


    private void Flip()
    {
        _facingDirection *= -1;
        _facingRight = !_facingRight;
        sprite.flipX = !sprite.flipX;
    }

}
