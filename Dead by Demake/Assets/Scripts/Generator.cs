using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public enum MachineState
{
    Off,
    On,
    Decoded
};

public enum SliderDir
{
    up,
    down
}

public class Generator : MonoBehaviour
{
    Animator anim;

    [SerializeField] SliderCol SliderCol;

    bool endTesting;
    public bool inRange;
    public bool getAction;
    public bool testing;
    public bool finalized;
    bool addFinalizedGenerator;
    bool getTest;
    

    float time;

    [SerializeField] Slider slidebar;
    [SerializeField] Slider sliderRandom;
    [SerializeField] Slider sliderbarProgess;
    [SerializeField] GameObject slidebarProgessObj;
    [SerializeField] GameObject sliderRandomObj;
    [SerializeField] GameObject sliderObj;
    [SerializeField] Manager Manager;

    public MachineState currState;

    public SliderDir currDir;


    PlayerController player;
    [SerializeField] SimpleAIMovement killer;


    void Awake()
    {
        getAction = false;
        getTest = false;
        sliderObj.SetActive(false);
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        Manager = GetComponentInParent<Manager>();
        killer = GameObject.FindGameObjectWithTag("Killer").GetComponent<SimpleAIMovement>();
        currState = MachineState.Off;
    }

    void Update()
    {
        if(finalized)
        {
            currState = MachineState.Decoded;
        }
        switch (currState)
        {
                //Gedor desligado
            case (MachineState.Off):
                anim.SetBool("On", false);
                statusReset();
                break;
                //Gedor ligado
            case (MachineState.On):
                anim.SetBool("On", true);

                if (player != null)
                {
                    if (player.currState != PlayerState.Captured)
                    {

                        player.currState = PlayerState.Decode;
                    }
                }

                if (sliderbarProgess.value == sliderbarProgess.maxValue)
                {
                    currState = MachineState.Decoded; 
                }

                time++;
                if (time > 30)
                { 
                sliderbarProgess.value+=20;
                    time = 0;
                }
                if (testing)
                {
                    StartCoroutine(DecodeOn());
                    testing = false;
                }
                break;
                //Gedor reparado
            case (MachineState.Decoded):
                
                if (!addFinalizedGenerator)
                {
                    Manager.AddGeneratorRepaired();
                    addFinalizedGenerator = true;
                }
                finalized = true;

                statusReset();

                sliderbarProgess.value = sliderbarProgess.maxValue;
                anim.SetTrigger("Decoded");
                break;
        }
    }

    void FixedUpdate()
    {
        switch (currState)
        {
            case (MachineState.On):
                DecodeUpdate();
                break;
        }
    }

    IEnumerator DecodeOn()
    {
        yield return new WaitForSeconds(4);

        if(currState == MachineState.On)
        { 
        sliderRandom.value = Random.Range(1, 400);
        sliderObj.SetActive(true);
        getTest = true;
        }
        yield return new WaitForSeconds(4);

        

        if (sliderObj.activeSelf)
        {
            sliderObj.SetActive(false);
            sliderbarProgess.value -= sliderbarProgess.value / 5;
            currState = MachineState.Off;
        }
        testing = true;
    }

    public void DecodeUpdate()
    {
        if (endTesting)
        {
            getTest = false;
            endTesting = false;
        }

        if (currDir == SliderDir.up && slidebar.value < slidebar.maxValue)
        {
            slidebar.value+=10;
        }
        else if (currDir == SliderDir.up && slidebar.value == slidebar.maxValue)
        {
            currDir = SliderDir.down;
        }
        else if (currDir == SliderDir.down && slidebar.value > slidebar.minValue)
        {
            slidebar.value-=10;
        }
        else if (currDir == SliderDir.down && slidebar.value == slidebar.minValue)
        {
            currDir = SliderDir.up;
        }
    }

    public void Decode(InputAction.CallbackContext context)
    {
        

        if(context.performed && inRange && currState == MachineState.Off)
        {
            slidebar.value = 0;
            testing = true;
            currState = MachineState.On;
            currDir = SliderDir.up;
        }

        else if(context.performed && inRange && currState == MachineState.On && getTest)
        {
            if(getAction)
            {
                endTesting = true;
                sliderObj.SetActive(false);
            }
            else
            {
                killer.isPlayerFound = true;
                killer.target = player.transform;
                killer.estadoAtual = BehaviourStates.PERSEGUINDO;
                endTesting = true;
                sliderObj.SetActive(false);
                sliderbarProgess.value -= sliderbarProgess.value / 5;
                currState = MachineState.Off;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Player"))
        {
            inRange = true;
            if(currState != MachineState.Decoded)
            { 
                player = collision.GetComponent<PlayerController>();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            player = null;
            inRange = false;
            collision.GetComponent<PlayerController>().currState = PlayerState.Walking;
            if (currState == MachineState.On)
            { 
                currState = MachineState.Off;
            }
            sliderObj.SetActive(false);
        }
    }

    void statusReset()
    {
        if (player != null)
        {
            if (player.GetComponent<Rigidbody2D>().velocity.x == 0 && player.GetComponent<Rigidbody2D>().velocity.y == 0)
            { 
                player.currState = PlayerState.Idle;
            }
            else
            {
                player.currState = PlayerState.Walking;
            }
        }
        
    }
}
