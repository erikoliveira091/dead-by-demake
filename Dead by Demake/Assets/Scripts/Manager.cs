using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Manager : MonoBehaviour
{
    [Header("M�ximo de geradores")]
    [SerializeField]
    int maxItems = 5;
    public GameObject ItemsPrefabs;
    public Transform[] RandomLocalsItems;
    public List<Transform> lista = new List<Transform>();

    private float tempo = 0.01f;

    [Header("Geradores Quebrados")]
    [SerializeField] int generatorsBroken;

    [SerializeField] TextMeshProUGUI generators;

    [SerializeField] GameObject gate;
    [SerializeField] Collider2D coll;

    [SerializeField] GameObject alarm;

    void Start()
    {
        SpawnItems();
    }

    private void Update()
    {
        generators.text = "" + generatorsBroken + " ";

        if(generatorsBroken == 0)
        {
            gate.SetActive(false);
            alarm.SetActive(true);
            coll.enabled = true;
        }
    }

    void SpawnItems()
    {
        StartCoroutine("Spawn");
        generatorsBroken = 5;
    }
    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(tempo);

        if (lista.Count < maxItems)
        {
            var index = Random.Range(0, RandomLocalsItems.Length);
            for (int i = 0; i < maxItems; i++)
            {
                if (!lista.Contains(RandomLocalsItems[index]))
                {
                    lista.Add(RandomLocalsItems[index]); 
                }
                else
                {
                    StartCoroutine("Spawn");
                }
            }
        }

        else
        {

            for (int x = 0; x < lista.Count; x++)
            {
                if (x < lista.Count)
                {
                    GameObject ob = Instantiate(ItemsPrefabs, lista[x].position, Quaternion.identity, transform);
                    StopCoroutine("Spawn");
                }
            }
        }
    }

    public void AddGeneratorRepaired()
    {
        generatorsBroken--;
    }
}
