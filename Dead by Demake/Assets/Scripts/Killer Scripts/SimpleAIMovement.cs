using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public enum BehaviourStates
{
    PROCURANDO,
    PERSEGUINDO,
    CAPTUROU
}

public class SimpleAIMovement : MonoBehaviour
{
    //Target que o Killer vai perseguir
    public Transform target;
    //Pontos de target para o modo de procura
    [SerializeField] Transform[] patrolPoints;
    [SerializeField] Transform GanchoPoint;
    //NavMeshAgent do player
    NavMeshAgent agent;
    //Raio de vis�o
    [SerializeField] private float raycastVision;
    //Layer em que ele consegue ver os colisores
    [SerializeField] LayerMask layer;

    
    //Tempo que ele continuar perseguindo mesmo sem ver
    [SerializeField] float seekingTime;
    //Tempo
    [SerializeField] private float timer;
    //Estado alternado do Killer (PROCURANDO, PERSEGUINDO, etc)
    public BehaviourStates estadoAtual;
    // Em desenvolvimento
    public float StunCooldown;
    // ''
    [SerializeField] public bool isPlayerGetted;
    //Diz se o player foi encontrado ou n�o
    public bool isPlayerFound;

    //Sprite
    [SerializeField] Transform sprite;
    //Controlador de anima��o do Killer
    [SerializeField] Animator anim;
    [SerializeField] SceneController sceneController;
    //Vetor de escala
    Vector3 newScale = new Vector3(1, 1, 1);

    void Start()
    {
        target = patrolPoints[Random.Range(0, patrolPoints.Length)];
        estadoAtual = BehaviourStates.PROCURANDO;

        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
    }

    void FixedUpdate()
    {
        
        SearchPlayer();
        StatesMachine();
        SpriteDirection();

        switch (estadoAtual)
        {
            case (BehaviourStates.PROCURANDO):
                Vagar();
                Mover();
                // vagar;
                break;

            case (BehaviourStates.PERSEGUINDO):
                Mover();
                Capturar();
                break;

            case (BehaviourStates.CAPTUROU):
                target = GanchoPoint;
                Mover();
                
                if (Vector2.Distance(this.transform.position, GanchoPoint.position) < 0.1f)
                {
                    sceneController.gameOver();
                }
                
                break;

        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, this.raycastVision);

       if(this.target != null)
        {
            Gizmos.DrawLine(this.transform.position, this.target.position);
        }
    }

    private void SearchPlayer()
    {
        //Cria um circulo invisivel, na posi��o atual do Killer, com o raio no valor da variavel raycastVision, somente nas layers setadas na variavel layer
        Collider2D colider = Physics2D.OverlapCircle(this.transform.position, this.raycastVision, layer);

        if (colider != null) //Jogador est� dentro da area de vis�o
        { 
            //Posi��o do Killer
            Vector2 positionNow = this.transform.position;
            //Posi��o do Player
            Vector2 positionTarget = colider.transform.position;
            //Dire��o do raio
            Vector2 direction = positionTarget - positionNow;
            //Normalizando a dire��o
            direction = direction.normalized;
            
            //Raio
            RaycastHit2D hit = Physics2D.Raycast(positionNow, direction);

            if(hit.transform != null)
            {
                if(hit.transform.CompareTag("Player"))
                { 
                    this.target = colider.transform;
                    isPlayerFound = true;
                    timer = 0;
                }
                else
                {
                    isPlayerFound = false;
                }
            }
            else
            {
                isPlayerFound = false;
            }

        }
        else
        {
            isPlayerFound = false;
        }


    }

    void Vagar()
    {
        if (transform.position == target.position || Vector3.Distance(transform.position, target.position) < 2)
        {
            target = patrolPoints[Random.Range(0, patrolPoints.Length)];
        }
    }

    private void Mover()
    {
        agent.SetDestination(target.position);
    }

    private void Capturar()
    {
        //print(Vector2.Distance(this.transform.position, target.transform.position));
        if (Vector2.Distance(this.transform.position, target.transform.position) < 1.5 && target.tag == "Player")
        {
            isPlayerGetted = true;
            print("Capturou");
        }
    }


    void StatesMachine()
    {

        switch (estadoAtual)
        {
            case (BehaviourStates.PROCURANDO):
                if (this.isPlayerFound)
                {
                    estadoAtual = BehaviourStates.PERSEGUINDO;
                }

                break;

            case (BehaviourStates.PERSEGUINDO):
                // -> procurando
                if (!this.isPlayerFound) 
                {
                    if (timer >= seekingTime)
                    {
                        target = patrolPoints[Random.Range(0, patrolPoints.Length)];

                        estadoAtual = BehaviourStates.PROCURANDO;
                        timer = 0;
                    }
                    else
                    {
                        timer += Time.deltaTime;
                    }

                }
                // -> Capturou
                if (isPlayerGetted)
                {
                    estadoAtual = BehaviourStates.CAPTUROU;
                }

                break;
        }

        

    }

    void SpriteDirection()
    {
        //Posi��o do Killer
        Vector2 positionNow = this.transform.position;
        //Posi��o do Target
        Vector2 positionTarget = target.position;
        //Dire��o do raio
        Vector2 direction = positionTarget - positionNow;
        //Vetor de escala

        if(direction.x > 0)
        {
            anim.SetBool("isWalking", true);
            if(newScale.x > 0)
            {
                newScale.x *= -1;
            }
            else
            {
                sprite.localScale = newScale;
            }
            
        }
        else if(direction.x < 0)
        {
            anim.SetBool("isWalking", true);
            if (newScale.x < 0)
            {
                newScale.x *= -1;
            }
            else
            {
                sprite.localScale = newScale;
            }
        }
        else
        {
            anim.SetBool("isWalking", false);
        }
    }
}
