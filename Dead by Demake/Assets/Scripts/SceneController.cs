using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class SceneController : MonoBehaviour
{
    public void startGame()
    {
        SceneManager.LoadScene("Main");
    }
    
    public void winGame()
    {
        SceneManager.LoadScene("Win");
    }

    public void gameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void menuGame()
    {
        SceneManager.LoadScene("Menu");
    }

    /*public void starAction(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            startGame();
        }
    }*/
}
