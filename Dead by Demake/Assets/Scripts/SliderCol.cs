using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderCol : MonoBehaviour
{
    Generator Generator;


    private void Awake()
    {
        Generator = GetComponentInParent<Generator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Generator.getAction = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Generator.getAction = false;
    }
}
