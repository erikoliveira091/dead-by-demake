using UnityEngine;
using UnityEngine.UI;

public class RandomArea : MonoBehaviour
{
    [SerializeField] Slider thisSlider;

    private void Start()
    {
        thisSlider.value = Random.Range(0, 100);
    }
}
